#include "PlayerInputComponent.hpp"

namespace WizardWars
{
	PlayerInputComponent::PlayerInputComponent(player1_t)
		: up(sf::Keyboard::Up)
		, down(sf::Keyboard::Down)
		, left(sf::Keyboard::Left)
		, right(sf::Keyboard::Right)
		, shootLaser(sf::Keyboard::RShift)
	{
	}

	PlayerInputComponent::PlayerInputComponent(player2_t)
		: up(sf::Keyboard::W)
		, down(sf::Keyboard::S)
		, left(sf::Keyboard::A)
		, right(sf::Keyboard::D)
		, shootLaser(sf::Keyboard::F)
	{
	}
}