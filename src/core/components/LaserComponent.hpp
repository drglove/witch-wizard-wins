#pragma once

namespace WizardWars
{
	struct LaserComponent
	{
		sf::Vector2f target;
		float strength;
	};
}