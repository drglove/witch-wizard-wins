#pragma once

#include <SFML/Graphics/CircleShape.hpp>

namespace WizardWars
{
    struct CircleRenderableComponent
    {
        sf::CircleShape circle;
    };
}
