#pragma once

#include <SFML/Graphics/Sprite.hpp>

namespace WizardWars
{
	struct SpriteComponent
	{
		sf::Sprite sprite;
	};
}
