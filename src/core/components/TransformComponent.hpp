#pragma once
#include <SFML/Graphics/Transformable.hpp>

namespace WizardWars
{
	struct PhysicsComponent;

	struct TransformComponent : public sf::Transformable
	{
		int depth = 0;
		void syncFromPhysics(PhysicsComponent const& physicsComponent);
	};
}
