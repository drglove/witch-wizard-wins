#include "PhysicsComponent.hpp"

namespace WizardWars
{
    PhysicsComponent::PhysicsComponent(b2BodyDef bodyDef)
        : bodyDef(bodyDef)
        , body(nullptr)
    {
    }
}