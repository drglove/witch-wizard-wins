#include "TransformComponent.hpp"
#include "PhysicsComponent.hpp"
#include "utilities/Macros.hpp"

namespace WizardWars
{
	void TransformComponent::syncFromPhysics(PhysicsComponent const& physicsComponent)
	{
		ASSERT(physicsComponent.body);
		b2Transform const& bodyTransform = physicsComponent.body->GetTransform();
		setPosition({ bodyTransform.p.x, bodyTransform.p.y });
		setRotation(bodyTransform.q.GetAngle() * 180.0f / b2_pi);
	}
}
