#pragma once

#include <Box2D/Box2D.h>

namespace WizardWars
{
    struct PhysicsComponent
    {
        PhysicsComponent(b2BodyDef bodyDef);
        b2BodyDef bodyDef;
        b2Body* body;
    };
}
