#pragma once

#include <SFML/Graphics/RectangleShape.hpp>

namespace WizardWars
{
    struct RectangleRenderableComponent
    {
        sf::RectangleShape rectangle;
    };
}
