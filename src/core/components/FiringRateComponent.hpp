#pragma once
#include <SFML/System/Time.hpp>

namespace WizardWars
{
	struct FiringRateComponent
	{
		FiringRateComponent(sf::Time rechargeTime);
		sf::Time rechargeTime;
		sf::Time timeWaited;
	};
}