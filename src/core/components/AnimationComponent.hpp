#pragma once

#include "core/resource-management/AssetCache.hpp"

namespace WizardWars
{
	class AnimationComponent
	{
	public:
		AnimationComponent(SpriteAnimations const* animations);

		inline SpriteAnimator& animator()
		{
			return mAnimator;
		}

	private:
		// Holds the texture rect data.
		SpriteAnimations const* mAnimations;

		// Holds the state of the currently being played animation
		SpriteAnimator mAnimator;
	};
}
