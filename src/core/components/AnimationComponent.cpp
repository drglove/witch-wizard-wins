#include "AnimationComponent.hpp"

namespace WizardWars
{
	AnimationComponent::AnimationComponent(SpriteAnimations const* animations)
		: mAnimations(animations)
		, mAnimator(*mAnimations)
	{
	}
}
