#pragma once

#include <queue>
#include <vector>

namespace WizardWars
{
	struct CharacterController
	{
		enum class Movement
		{
			UP,
			DOWN,
			RIGHT,
			LEFT
		};

		std::queue<Movement> moves;
	};
}
