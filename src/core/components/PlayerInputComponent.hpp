#pragma  once

#include <SFML/Window/Keyboard.hpp>

namespace WizardWars
{
	struct PlayerInputComponent
	{
		struct player1_t final {};
		struct player2_t final {};

		PlayerInputComponent(player1_t);
		PlayerInputComponent(player2_t);

		sf::Keyboard::Key up;
		sf::Keyboard::Key down;
		sf::Keyboard::Key left;
		sf::Keyboard::Key right;
		sf::Keyboard::Key shootLaser;
	};
}