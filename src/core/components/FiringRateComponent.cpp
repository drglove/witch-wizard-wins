#include "FiringRateComponent.hpp"

namespace WizardWars
{
	FiringRateComponent::FiringRateComponent(sf::Time rechargeTime)
		: rechargeTime(rechargeTime)
		, timeWaited(sf::Time::Zero)
	{
	}
}