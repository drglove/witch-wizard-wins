#include "Game.hpp"

#include "resource-management/AssetLoader.hpp"
#include "components/TransformComponent.hpp"
#include "components/SpriteComponent.hpp"
#include "components/AnimationComponent.hpp"
#include "components/PlayerInputComponent.hpp"
#include "components/CharacterController.hpp"
#include "components/PhysicsComponent.hpp"

#include "entity-factories/beard-wizard/BeardWizardFactory.hpp"

#include "constants/RenderingConstants.hpp"
#include "utilities/RenderingUtilities.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/Window/Keyboard.hpp>

#include <Box2D/Box2D.h>

namespace WizardWars
{
	Game::Game()
		: mRenderer(mRegistry, mMessageDispatcher)
		, mAnimator(mRegistry, mMessageDispatcher)
		, mPlayerInput(mRegistry, mMessageDispatcher)
		, mPhysicsWorld(mRegistry, mMessageDispatcher)
		, mMover(mRegistry, mMessageDispatcher)
		, mShooter(mRegistry, mMessageDispatcher)
		, mPhysicsDebugDraw(mWindow)
	{
	}

	Game& Game::Instance()
	{
		static Game theInstance;
		return theInstance;
	}

	Registry& Game::EntityRegistry()
	{
		return Instance().mRegistry;
	}

	AssetCache const& Game::Assets()
	{
		return Instance().mAssets;
	}

	void Game::run()
	{
		initWindow();
		loadAssets();

		auto background = mRegistry.create();
		{
			auto& transform = mRegistry.assign<TransformComponent>(background);
			transform.depth = -1;
			mRegistry.assign<SpriteComponent>(background, sf::Sprite(mAssets.textureCache["media/backgrounds/background-sample.jpg"]));
		}

		auto wizard = BeardWizardFactory::Create(BeardWizardFactory::InitialPlayerControl::PLAYER_1_CONTROLS);
		{
			// Kick off some animation for testing.
			AnimationComponent& wizardAnims = mRegistry.get<AnimationComponent>(wizard);
			wizardAnims.animator().play() << thor::Playback::loop("Walk-Left");
		}

		auto opponentWizard = BeardWizardFactory::Create(BeardWizardFactory::InitialPlayerControl::NOT_CONTROLLED);
		{
			// Kick off some animation for testing.
			AnimationComponent& wizardAnims = mRegistry.get<AnimationComponent>(opponentWizard);
			wizardAnims.animator().play() << thor::Playback::loop("Idle");
		}

		// Game loop
		sf::Clock clock;
		sf::Time const timePerFrame = sf::seconds(1.0f / 60.0f);
		sf::Time timeSinceUpdate = timePerFrame;
		while (mWindow.isOpen())
		{
			handleEvents();

			// Run the simulation for as many frames as required to keep 60Hz.
			sf::Time const dt = clock.restart();
			timeSinceUpdate += dt;
			while (timeSinceUpdate >= timePerFrame)
			{
				timeSinceUpdate -= timePerFrame;
				frame(timePerFrame);
			}

			{
				// Have the camera follow the player
				TransformComponent const& pos = mRegistry.get<TransformComponent>(wizard);
				sf::View playerView = mWindow.getDefaultView();
				sf::Vector2f const center = WorldToScreenCoords(mWindow).transformPoint(pos.getPosition());
				playerView.setCenter(center);
				mWindow.setView(playerView);
			}

			draw();
		}
	}

	void Game::initWindow()
	{
		sf::VideoMode const videoMode(NativeResolutionWidth, NativeResolutionHeight);
		sf::String const title = "Wizard Wars";
		mWindow.create(videoMode, title, sf::Style::Titlebar | sf::Style::Close);
	}

	void Game::loadAssets()
	{
		AssetLoader loader(mAssets);
		loader.load("media/Assets.xml");
	}

	void Game::handleEvents()
	{
		sf::Event event;
		while (mWindow.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
			{
				mWindow.close();
				break;
			}
			case sf::Event::KeyPressed:
			{
				static bool enableDebugDraw = false;
				if (event.key.code == sf::Keyboard::P)
				{
					enableDebugDraw = !enableDebugDraw;
					mPhysicsWorld.setDebugDraw(enableDebugDraw ? &mPhysicsDebugDraw : nullptr);
				}
				break;
			}
			default:
				break;
			}
		}
	}

	void Game::frame(sf::Time dt)
	{
		mAnimator.animate(dt);
		mPlayerInput.update();
		mMover.move(dt);
		mPhysicsWorld.update(dt);
		mShooter.update(dt);

		mMessageDispatcher.update();
	}

	void Game::draw()
	{
		mRenderer.render(mWindow);
		mPhysicsWorld.debugDraw();
		mWindow.display();
	}

}
