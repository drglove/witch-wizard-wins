#include "ShootingSystem.hpp"
#include "core/events/ShootingEvents.hpp"
#include "core/components/FiringRateComponent.hpp"
#include <core/components/TransformComponent.hpp>
#include "entity-factories/laser/LaserFactory.hpp"

namespace WizardWars
{
	ShootingSystem::ShootingSystem(Registry& entityRegistry, Dispatcher& messageDispatcher)
		: System(entityRegistry, messageDispatcher)
		, shootRechargeTime(sf::seconds(0.5f))
	{
		dispatcher().sink<ShootEvent>().connect<ShootingSystem, &ShootingSystem::fire>(this);
	}

	ShootingSystem::~ShootingSystem()
	{
		dispatcher().sink<ShootEvent>().disconnect(this);
	}

	void ShootingSystem::update(sf::Time dt)
	{
		auto view = registry().view<FiringRateComponent>();

		auto cooldown = [&view, this, dt](Entity entity)
		{
		    auto& firingRate = registry().get<FiringRateComponent>(entity);
		    firingRate.timeWaited += dt;
		    if (firingRate.timeWaited >= firingRate.rechargeTime)
				registry().remove<FiringRateComponent>(entity);
		};

		for (auto entity : view)
			cooldown(entity);
	}

	void ShootingSystem::fire(ShootEvent const& event)
	{
	    bool const canShoot = !registry().has<FiringRateComponent>(event.shooter);
	    if (!canShoot)
			return;

	    registry().assign<FiringRateComponent>(event.shooter, shootRechargeTime);
	    sf::Vector2f const source = registry().has<TransformComponent>(event.shooter) ?
	            registry().get<TransformComponent>(event.shooter).getPosition() : sf::Vector2f{0.0f, 0.0f};
	    sf::Vector2f const target = event.target;
	    LaserFactory::Create(source, target);
	}
}