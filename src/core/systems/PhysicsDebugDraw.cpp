#include "PhysicsDebugDraw.hpp"
#include "constants/RenderingConstants.hpp"
#include "utilities/RenderingUtilities.hpp"
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>

namespace WizardWars
{
    PhysicsDebugDraw::PhysicsDebugDraw(sf::RenderTarget& renderTarget)
        : mRenderTarget(renderTarget)
    {
        SetFlags(b2Draw::e_shapeBit |
                 b2Draw::e_jointBit |
                 b2Draw::e_aabbBit |
                 b2Draw::e_pairBit |
                 b2Draw::e_centerOfMassBit);
    }

    void PhysicsDebugDraw::DrawPolygon(b2Vec2 const* vertices, int32 vertexCount, b2Color const& color)
    {
        sf::ConvexShape polygon;
        polygon.setPointCount(vertexCount);
        for (int i = 0; i < vertexCount; ++i)
            polygon.setPoint(i, b2VecTosfVec(vertices[i]));
        polygon.setOutlineThickness(-1.0f);
        polygon.setFillColor(sf::Color::Transparent);
        polygon.setOutlineColor(b2ColorTosfColor(color));

        drawPhysicsObject(polygon);
    }

    void PhysicsDebugDraw::DrawSolidPolygon(b2Vec2 const* vertices, int32 vertexCount, b2Color const& color)
    {
        sf::ConvexShape polygon;
        polygon.setPointCount(vertexCount);
        for (int i = 0; i < vertexCount; ++i)
            polygon.setPoint(i, b2VecTosfVec(vertices[i]));
        polygon.setOutlineThickness(-1.0f);
        polygon.setFillColor(b2ColorTosfColor(color, 0.5f));
        polygon.setOutlineColor(b2ColorTosfColor(color));

        drawPhysicsObject(polygon);
    }

    void PhysicsDebugDraw::DrawCircle(b2Vec2 const& center, float32 radius, b2Color const& color)
    {
        sf::CircleShape circle;
        sf::Vector2f const origin = b2VecTosfVec(b2Vec2(radius, radius));
        circle.setRadius(origin.x);
        circle.setOrigin(origin);
        circle.setPosition(b2VecTosfVec(center));
        circle.setOutlineThickness(-1.0f);
        circle.setFillColor(sf::Color::Transparent);
        circle.setOutlineColor(b2ColorTosfColor(color));

        drawPhysicsObject(circle);
    }

    void PhysicsDebugDraw::DrawSolidCircle(b2Vec2 const& center, float32 radius, b2Vec2 const& axis, b2Color const& color)
    {
        sf::CircleShape circle;
        sf::Vector2f const origin = b2VecTosfVec(b2Vec2(radius, radius));
        circle.setRadius(origin.x);
        circle.setOrigin(origin);
        circle.setPosition(b2VecTosfVec(center));
        circle.setOutlineThickness(-1.0f);
        circle.setFillColor(b2ColorTosfColor(color, 0.5f));
        circle.setOutlineColor(b2ColorTosfColor(color));

        b2Vec2 const endpoint = center + radius * axis;
        sf::Vertex const line[] =
        {
            {b2VecTosfVec(center), b2ColorTosfColor(color)},
            {b2VecTosfVec(endpoint), b2ColorTosfColor(color)}
        };

        drawPhysicsObject(circle);
        drawPhysicsLine(line, 2);
    }

    void PhysicsDebugDraw::DrawSegment(b2Vec2 const& p1, b2Vec2 const& p2, b2Color const& color)
    {
        sf::Vertex const line[] =
        {
            {b2VecTosfVec(p1), b2ColorTosfColor(color)},
            {b2VecTosfVec(p2), b2ColorTosfColor(color)}
        };

        drawPhysicsLine(line, 2);
    }

    void PhysicsDebugDraw::DrawTransform(b2Transform const& xf)
    {
        float const transformLength = 0.5f;

        b2Vec2 const xStart = xf.p;
        b2Vec2 const xEnd = xStart + transformLength * xf.q.GetXAxis();
        sf::Vertex const xAxis[] =
        {
            {b2VecTosfVec(xStart), sf::Color::Red},
            {b2VecTosfVec(xEnd), sf::Color::Red}
        };

        b2Vec2 const yStart = xf.p;
        b2Vec2 const yEnd = yStart + transformLength * xf.q.GetYAxis();
        sf::Vertex const yAxis[] =
        {
            {b2VecTosfVec(yStart), sf::Color::Green},
            {b2VecTosfVec(yEnd), sf::Color::Green}
        };

        drawPhysicsLine(xAxis, 2);
        drawPhysicsLine(yAxis, 2);
    }

    void PhysicsDebugDraw::DrawPoint(b2Vec2 const& p, float32 size, b2Color const& color)
    {
        sf::CircleShape circle;
        sf::Vector2f const origin = b2VecTosfVec(b2Vec2(size, size));
        circle.setRadius(origin.x);
        circle.setOrigin(origin);
        circle.setPosition(b2VecTosfVec(p));
        circle.setFillColor(b2ColorTosfColor(color));

		drawPhysicsObject(circle);
    }

    sf::Vector2f PhysicsDebugDraw::b2VecTosfVec(b2Vec2 const& vec)
    {
        float const scale = PhysicsWorldToRenderScale * static_cast<float>(mRenderTarget.getSize().y) / NativeResolutionHeight;
        return scale * sf::Vector2f(vec.x, vec.y);
    }

    sf::Color PhysicsDebugDraw::b2ColorTosfColor(b2Color const& color, float alpha)
    {
        return {sf::Uint8(color.r * 255), sf::Uint8(color.g * 255), sf::Uint8(color.b * 255), sf::Uint8(alpha * 255)};
    }

	void PhysicsDebugDraw::drawPhysicsObject(sf::Drawable const& drawable)
	{
		mRenderTarget.draw(drawable, physicsRenderState());
	}

	void PhysicsDebugDraw::drawPhysicsLine(sf::Vertex const* vertices, std::size_t vertexCount)
	{
		mRenderTarget.draw(vertices, vertexCount, sf::Lines, physicsRenderState());
	}

	sf::RenderStates PhysicsDebugDraw::physicsRenderState() const
	{
		sf::RenderStates renderStates = sf::RenderStates::Default;
		sf::Transform const worldToScreen = WorldToScreenCoords(mRenderTarget);
		sf::Transform undoPhysicsScaleApplied;
		float const invScale = 1.0f / (PhysicsWorldToRenderScale * static_cast<float>(mRenderTarget.getSize().y) / NativeResolutionHeight);
		undoPhysicsScaleApplied.scale({ invScale, invScale });
		renderStates.transform = worldToScreen * undoPhysicsScaleApplied * renderStates.transform;
		return renderStates;
	}
}
