#include "AnimationSystem.hpp"
#include "core/components/AnimationComponent.hpp"
#include "core/components/SpriteComponent.hpp"

namespace WizardWars
{
	AnimationSystem::AnimationSystem(Registry& entityRegistry, Dispatcher& messageDispatcher)
		: System(entityRegistry, messageDispatcher)
	{
		registry().prepare<AnimationComponent, SpriteComponent>();
	}

	void AnimationSystem::animate(sf::Time dt)
	{
		auto view = registry().view<AnimationComponent, SpriteComponent>(entt::persistent_t{});

		for (auto entity : view)
		{
			auto& animationComp = view.get<AnimationComponent>(entity);
			auto& spriteComp = view.get<SpriteComponent>(entity);

			SpriteAnimator& animator = animationComp.animator();
			animator.update(dt);
			animator.animate(spriteComp.sprite);
		}
	}
}
