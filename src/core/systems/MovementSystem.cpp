#include "MovementSystem.hpp"
#include "core/components/TransformComponent.hpp"
#include "core/components/CharacterController.hpp"
#include "core/components/PhysicsComponent.hpp"
#include <SFML/System/Vector2.hpp>

namespace WizardWars
{
	MovementSystem::MovementSystem(Registry& entityRegistry, Dispatcher& messageDispatcher)
		: System(entityRegistry, messageDispatcher)
	{
		registry().prepare<TransformComponent, CharacterController>();
	}

	void MovementSystem::move(sf::Time dt)
	{
		auto view = registry().view<TransformComponent, CharacterController>(entt::persistent_t{});

		for (auto entity : view)
		{
			auto& characterController = view.get<CharacterController>(entity);
			auto& transformComponent = view.get<TransformComponent>(entity);

			sf::Vector2f step;
			auto& moves = characterController.moves;
			while (!moves.empty())
			{
				CharacterController::Movement move = moves.front();
				switch (move)
				{
				case CharacterController::Movement::UP:
					step += {  0.0f,  1.0f };
					break;
				case CharacterController::Movement::DOWN:
					step += {  0.0f, -1.0f };
					break;
				case CharacterController::Movement::RIGHT:
					step += {  1.0f,  0.0f };
					break;
				case CharacterController::Movement::LEFT:
					step += { -1.0f,  0.0f };
					break;
				}
				moves.pop();
			}

			float const speed = 8.0f;
			step = step * speed * dt.asSeconds();

			if (!registry().has<PhysicsComponent>(entity))
			{
				transformComponent.move(step);
			}
			else
			{
				auto& p = registry().get<PhysicsComponent>(entity);
				p.body->ApplyLinearImpulseToCenter({ step.x, step.y }, true);
			}
		}
	}
}
