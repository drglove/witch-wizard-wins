#include "RenderingSystem.hpp"
#include "core/components/TransformComponent.hpp"
#include "core/components/SpriteComponent.hpp"
#include "core/components/CircleRenderableComponent.hpp"
#include "core/components/RectangleRenderableComponent.hpp"
#include "constants/RenderingConstants.hpp"
#include "utilities/RenderingUtilities.hpp"

namespace WizardWars
{
	RenderingSystem::RenderingSystem(Registry& entityRegistry, Dispatcher& messageDispatcher)
		: System(entityRegistry, messageDispatcher)
	{
		registry().prepare<TransformComponent, SpriteComponent>();
		registry().prepare<TransformComponent, CircleRenderableComponent>();
		registry().prepare<TransformComponent, RectangleRenderableComponent>();
	}

	void RenderingSystem::render(sf::RenderTarget& renderTarget)
	{
		addSpritesToQueue();
		addShapesToQueue();

		std::sort(mRenderQueue.begin(), mRenderQueue.end(), [](RenderInstruction const& lhs, RenderInstruction const& rhs)
		{
		    return lhs.depth < rhs.depth;
		});

		renderTarget.clear();
		for (RenderInstruction const& instruction : mRenderQueue)
			executeRenderInstruction(instruction, renderTarget);

		mRenderQueue.clear();
	}

	void RenderingSystem::addSpritesToQueue()
	{
		auto view = registry().view<TransformComponent, SpriteComponent>(entt::persistent_t{});
		for (auto entity : view)
		{
			auto const& transformComp = view.get<TransformComponent>(entity);
			auto const& spriteComp = view.get<SpriteComponent>(entity);

			sf::Sprite const& sprite = spriteComp.sprite;

			// This flips our sprites about their centre.
			sf::IntRect const rect = sprite.getTextureRect();
			sf::Transformable scaleAndFlip;
			scaleAndFlip.setOrigin({ rect.width / 2.0f, rect.height / 2.0f });
			scaleAndFlip.setScale({ TextureWorldToRenderScale / PhysicsWorldToRenderScale, -TextureWorldToRenderScale / PhysicsWorldToRenderScale });
			sf::Transform const transform = transformComp.getTransform() * scaleAndFlip.getTransform();

			mRenderQueue.push_back({ &sprite, transform, transformComp.depth });
		}
	}

	void RenderingSystem::addShapesToQueue()
	{
		{
			// Circles
			auto view = registry().view<TransformComponent, CircleRenderableComponent>(entt::persistent_t{});
			for (auto entity : view)
			{
				auto const& transformComp = view.get<TransformComponent>(entity);
				auto const& circleComp = view.get<CircleRenderableComponent>(entity);

				sf::CircleShape const& circle = circleComp.circle;
				sf::Transformable flip;
				flip.setOrigin({ circle.getRadius() / 2.0f, circle.getRadius() / 2.0f });
				flip.setScale({ 1.0f, -1.0f });
				sf::Transform const transform = transformComp.getTransform() * flip.getTransform();

				mRenderQueue.push_back({&circle, transform, transformComp.depth});
			}
		}

		{
			// Rectangles
			auto view = registry().view<TransformComponent, RectangleRenderableComponent>(entt::persistent_t{});
			for (auto entity : view)
			{
				auto const& transformComp = view.get<TransformComponent>(entity);
				auto const& rectangleComp = view.get<RectangleRenderableComponent>(entity);

				sf::RectangleShape const& rect = rectangleComp.rectangle;
				sf::Transformable flip;
				flip.setOrigin(rect.getSize() / 2.0f);
				flip.setScale({ 1.0f, -1.0f });
				sf::Transform const transform = transformComp.getTransform() * flip.getTransform();

				mRenderQueue.push_back({&rect, transform, transformComp.depth});
			}
		}
	}

	void RenderingSystem::executeRenderInstruction(RenderInstruction const& instruction, sf::RenderTarget& renderTarget)
	{
	    sf::Transform const transform = WorldToScreenCoords(renderTarget) * instruction.worldCoords;
	    renderTarget.draw(*instruction.drawable, transform);
	}
}
