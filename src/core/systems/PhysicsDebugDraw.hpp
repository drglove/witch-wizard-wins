#pragma once

#include <Box2D/Box2D.h>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Color.hpp>

namespace WizardWars
{
    class PhysicsDebugDraw : public b2Draw
    {
    public:
        PhysicsDebugDraw(sf::RenderTarget& renderTarget);
        void DrawPolygon(b2Vec2 const* vertices, int32 vertexCount, b2Color const& color) override;
        void DrawSolidPolygon(b2Vec2 const* vertices, int32 vertexCount, b2Color const& color) override;
        void DrawCircle(b2Vec2 const& center, float32 radius, b2Color const& color) override;
        void DrawSolidCircle(b2Vec2 const& center, float32 radius, b2Vec2 const& axis, b2Color const& color) override;
        void DrawSegment(b2Vec2 const& p1, b2Vec2 const& p2, b2Color const& color) override;
        void DrawTransform(b2Transform const& xf) override;
        void DrawPoint(b2Vec2 const& p, float32 size, b2Color const& color) override;

    private:
        sf::Vector2f b2VecTosfVec(b2Vec2 const& vec);
        sf::Color b2ColorTosfColor(b2Color const& color, float alpha = 1.0f);

        void drawPhysicsObject(sf::Drawable const& drawable);
        void drawPhysicsLine(sf::Vertex const* vertices, std::size_t vertexCount);
        sf::RenderStates physicsRenderState() const;

        sf::RenderTarget& mRenderTarget;
    };
}
