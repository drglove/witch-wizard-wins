#pragma once

#include "System.hpp"
#include <SFML/System/Time.hpp>

namespace WizardWars
{
	//////////////////////////////////////////////////////////////////////////
	// Animation system:
	//	uses entities with the following components
	//  - AnimationComponent
	//  - SpriteComponent
	//////////////////////////////////////////////////////////////////////////
	class AnimationSystem : private System
	{
	public:
		AnimationSystem(Registry& entityRegistry, Dispatcher& messageDispatcher);
		void animate(sf::Time dt);
	};
}
