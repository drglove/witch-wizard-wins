#pragma once

#include "System.hpp"
#include <SFML/Graphics/RenderTarget.hpp>

namespace WizardWars
{
	//////////////////////////////////////////////////////////////////////////
	// Render system:
	//	uses entities with the following components
	//  - TransformComponent
	//  - SpriteComponent
	//  - CircleRenderableComponent
	//  - RectangleRenderableComponent
	//////////////////////////////////////////////////////////////////////////
	class RenderingSystem : private System
	{
	public:
		RenderingSystem(Registry& entityRegistry, Dispatcher& messageDispatcher);
		void render(sf::RenderTarget& renderTarget);

	private:

		struct RenderInstruction
		{
			sf::Drawable const* drawable;
			sf::Transform worldCoords;
			int depth;
		};

		void addSpritesToQueue();
		void addShapesToQueue();
		void executeRenderInstruction(RenderInstruction const& instruction, sf::RenderTarget& renderTarget);

		std::vector<RenderInstruction> mRenderQueue;
	};
}
