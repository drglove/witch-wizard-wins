#pragma once

#include "System.hpp"
#include "core/events/ShootingEvents.hpp"
#include <SFML/System/Time.hpp>

namespace WizardWars
{
	//////////////////////////////////////////////////////////////////////////
	// Laser firing system:
	//	uses entities with the following components
	//  - CharacterController
	//  - TransformComponent
	//////////////////////////////////////////////////////////////////////////
	class ShootingSystem : private System
	{
	public:
		ShootingSystem(Registry& entityRegistry, Dispatcher& messageDispatcher);
		~ShootingSystem();
		void update(sf::Time dt);
	private:
		void fire(ShootEvent const& event);
		sf::Time shootRechargeTime;
	};
}