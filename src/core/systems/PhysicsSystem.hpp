#pragma once

#include "System.hpp"
#include <Box2D/Box2D.h>
#include <SFML/System/Time.hpp>

namespace WizardWars
{
	//////////////////////////////////////////////////////////////////////////
	// Physics system:
	//	uses entities with the following components
	//  - PhysicsComponent
	//////////////////////////////////////////////////////////////////////////
	class PhysicsSystem : private System
	{
	public:
		PhysicsSystem(Registry& entityRegistry, Dispatcher& messageDispatcher);
		~PhysicsSystem();
		void update(sf::Time dt);

		void setDebugDraw(b2Draw* debugDraw);
		void debugDraw();

		void setContactListener(b2ContactListener* listener);

	private:
		void onPhysicsComponentConstructed(Registry& registry, Entity entity);
		void onPhysicsComponentDestroyed(Registry& registry, Entity entity);
		void stepPhysicsWorld(sf::Time dt);

		b2Body* createBody(b2BodyDef bodyDef);
		void destroyBody(b2Body* body);

		b2World mPhysicsWorld;
	};
}
