#include "System.hpp"

namespace WizardWars
{
	System::System(Registry& entityRegistry, Dispatcher& messageDispatcher)
		: mRegistry(entityRegistry)
		, mMessageDispatcher(messageDispatcher)
	{
	}
}
