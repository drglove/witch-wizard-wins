#include "PlayerInputSystem.hpp"
#include "core/components/PlayerInputComponent.hpp"
#include "core/components/CharacterController.hpp"
#include "core/events/ShootingEvents.hpp"
#include <SFML/Window/Keyboard.hpp>

namespace WizardWars
{
	PlayerInputSystem::PlayerInputSystem(Registry& entityRegistry, Dispatcher& messageDispatcher)
		: System(entityRegistry, messageDispatcher)
	{
		registry().prepare<PlayerInputComponent, CharacterController>();
	}

	void PlayerInputSystem::update()
	{
		auto view = registry().view<PlayerInputComponent, CharacterController>(entt::persistent_t{});
		for (auto entity : view)
		{
			auto const& input = view.get<PlayerInputComponent>(entity);
			auto& cc = view.get<CharacterController>(entity);

			if (sf::Keyboard::isKeyPressed(input.up))
				cc.moves.push(CharacterController::Movement::UP);
			if (sf::Keyboard::isKeyPressed(input.down))
				cc.moves.push(CharacterController::Movement::DOWN);
			if (sf::Keyboard::isKeyPressed(input.left))
				cc.moves.push(CharacterController::Movement::LEFT);
			if (sf::Keyboard::isKeyPressed(input.right))
				cc.moves.push(CharacterController::Movement::RIGHT);

			if (sf::Keyboard::isKeyPressed(input.shootLaser))
			{
			    //TODO: Nick: Get an appropriate target here.
			    sf::Vector2f const target = {0.0f, 0.0f};
				dispatcher().enqueue<ShootEvent>(entity, target);
			}
		}
	}
}