#pragma once

#include "core/entities/Registry.hpp"
#include "core/entities/MessageDispatcher.hpp"

namespace WizardWars
{
	// Abstract class that our systems can use so they have ready access
	// to the entity registry.
	class System
	{
	protected:
		System(Registry& entityRegistry, Dispatcher& messageDispatcher);

		inline Registry& registry() { return mRegistry; }
		inline Registry const& registry() const { return mRegistry; }

		inline Dispatcher& dispatcher() { return mMessageDispatcher; }
		inline Dispatcher const& dispatcher() const { return mMessageDispatcher; }

	private:
		Registry& mRegistry;
		Dispatcher& mMessageDispatcher;
	};
}
