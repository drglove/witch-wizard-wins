#pragma once

#include "System.hpp"

namespace WizardWars
{
	//////////////////////////////////////////////////////////////////////////
	// PlayerInput system:
	//	uses entities with the following components
	//  - PlayerInputComponent
	//  - CharacterController
	//////////////////////////////////////////////////////////////////////////
	class PlayerInputSystem : private System
	{
	public:
		PlayerInputSystem(Registry& entityRegistry, Dispatcher& messageDispatcher);
		void update();
	};
}