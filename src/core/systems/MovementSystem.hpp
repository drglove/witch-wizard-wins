#pragma once

#include "System.hpp"
#include <SFML/System/Time.hpp>

namespace WizardWars
{
	//////////////////////////////////////////////////////////////////////////
	// Movement system:
	//	uses entities with the following components
	//  - CharacterController
	//  - TransformComponent
	//////////////////////////////////////////////////////////////////////////
	class MovementSystem : private System
	{
	public:
		MovementSystem(Registry& entityRegistry, Dispatcher& messageDispatcher);
		void move(sf::Time dt);
	};
}
