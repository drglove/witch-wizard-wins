#include "PhysicsSystem.hpp"
#include "core/components/PhysicsComponent.hpp"
#include "core/components/TransformComponent.hpp"
#include "utilities/Macros.hpp"

namespace WizardWars
{
	PhysicsSystem::PhysicsSystem(Registry& entityRegistry, Dispatcher& messageDispatcher)
		: System(entityRegistry, messageDispatcher)
		, mPhysicsWorld({0.0f, 0.0f})
	{
		registry().construction<PhysicsComponent>().connect<PhysicsSystem, &PhysicsSystem::onPhysicsComponentConstructed>(this);
		registry().destruction<PhysicsComponent>().connect<PhysicsSystem, &PhysicsSystem::onPhysicsComponentDestroyed>(this);
		registry().prepare<TransformComponent, PhysicsComponent>();
	}

	PhysicsSystem::~PhysicsSystem()
	{
		registry().construction<PhysicsComponent>().disconnect<PhysicsSystem, &PhysicsSystem::onPhysicsComponentConstructed>(this);
		registry().destruction<PhysicsComponent>().disconnect<PhysicsSystem, &PhysicsSystem::onPhysicsComponentDestroyed>(this);
	}

	void PhysicsSystem::update(sf::Time dt)
	{
		stepPhysicsWorld(dt);

		auto view = registry().view<TransformComponent, PhysicsComponent>(entt::persistent_t{});
		for (auto entity : view)
		{
			auto& transformComp = view.get<TransformComponent>(entity);
			auto const& physicsComp = view.get<PhysicsComponent>(entity);
			bool const isActive = physicsComp.body->IsActive();
			if (isActive)
				transformComp.syncFromPhysics(physicsComp);
		}
	}

	void PhysicsSystem::stepPhysicsWorld(sf::Time dt)
	{
		// These are the defaults recommended by Box2D.
		sf::Int32 const velocityIterations = 8;
		sf::Int32 const positionIterations = 3;
		mPhysicsWorld.Step(dt.asSeconds(), velocityIterations, positionIterations);
	}

	void PhysicsSystem::setDebugDraw(b2Draw* debugDraw)
	{
		mPhysicsWorld.SetDebugDraw(debugDraw);
	}

	void PhysicsSystem::debugDraw()
	{
		mPhysicsWorld.DrawDebugData();
	}

	void PhysicsSystem::setContactListener(b2ContactListener* listener)
	{
		mPhysicsWorld.SetContactListener(listener);
	}

	b2Body* PhysicsSystem::createBody(b2BodyDef bodyDef)
	{
		return mPhysicsWorld.CreateBody(&bodyDef);
	}

	void PhysicsSystem::destroyBody(b2Body* body)
	{
		mPhysicsWorld.DestroyBody(body);
	}

	void PhysicsSystem::onPhysicsComponentConstructed(Registry& registry, Entity entity)
	{
		PhysicsComponent& p = registry.get<PhysicsComponent>(entity);
		p.body = createBody(p.bodyDef);
		uintptr_t entityStashable = entity;
		p.body->SetUserData(reinterpret_cast<void*>(entityStashable));
	}

	void PhysicsSystem::onPhysicsComponentDestroyed(Registry& registry, Entity entity)
	{
		PhysicsComponent& p = registry.get<PhysicsComponent>(entity);
		ASSERT(reinterpret_cast<uintptr_t>(p.body->GetUserData()) == static_cast<uintptr_t>(entity));
		destroyBody(p.body);
		p.body = nullptr;
	}
}
