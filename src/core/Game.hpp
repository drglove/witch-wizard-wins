#pragma once
#include "resource-management/AssetCache.hpp"

#include "entities/Registry.hpp"
#include "entities/MessageDispatcher.hpp"
#include "systems/RenderingSystem.hpp"
#include "systems/AnimationSystem.hpp"
#include "systems/MovementSystem.hpp"
#include "systems/PlayerInputSystem.hpp"
#include "systems/PhysicsSystem.hpp"
#include "systems/ShootingSystem.hpp"
#include "systems/PhysicsDebugDraw.hpp"

#include <SFML/Graphics/RenderWindow.hpp>


namespace WizardWars
{
	class Game
	{
	public:
		static Game& Instance();
		void run();

		static Registry& EntityRegistry();
		static AssetCache const& Assets();

	private:
		Game();
		void initWindow();
		void loadAssets();
		void handleEvents();
		void frame(sf::Time dt);
		void draw();

		sf::RenderWindow mWindow;
		AssetCache mAssets;

		Registry mRegistry;
		Dispatcher mMessageDispatcher;

		RenderingSystem mRenderer;
		AnimationSystem mAnimator;
		PlayerInputSystem mPlayerInput;
		PhysicsSystem mPhysicsWorld;
		MovementSystem mMover;
		ShootingSystem mShooter;

		PhysicsDebugDraw mPhysicsDebugDraw;
	};
}
