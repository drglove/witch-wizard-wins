#pragma once

#include <SFML/System.hpp>
#include "core/entities/Registry.hpp"

namespace WizardWars
{
    struct ShootEvent
    {
        Entity shooter;
        sf::Vector2f target;
    };
}
