#include "AssetLoader.hpp"
#include "SpriteAssetHandler.hpp"
#include "utilities/Macros.hpp"

#include <algorithm>
#include <memory>

using namespace tinyxml2;

namespace WizardWars
{
	AssetLoader::AssetLoader(AssetCache& assetCache)
	{
		registerHandler(std::make_unique<SpriteAssetHandler>(assetCache));
	}

	void AssetLoader::registerHandler(AssetHandler::Ptr handler)
	{
		bool const alreadyHandled = std::any_of(mHandlers.begin(), mHandlers.end(), [&handler](auto const& other)
		{
			return handler->elementName() == other->elementName();
		});
		ASSERT(!alreadyHandled);
		(void)alreadyHandled;
		mHandlers.push_back(std::move(handler));
	}

	void AssetLoader::load(std::string const& rootFile)
	{
		tinyxml2::XMLDocument doc;
		XMLError eResult = doc.LoadFile(rootFile.c_str());
		ASSERT(eResult == XML_SUCCESS);
		(void)eResult;
		doc.Accept(this);
	}

	bool AssetLoader::VisitEnter(XMLElement const& element, XMLAttribute const* firstAttribute)
	{
		XMLVisitor* visitor = getVisitor(element);
		if (visitor)
			return visitor->VisitEnter(element, firstAttribute);
		return true;
	}

	bool AssetLoader::VisitExit(XMLElement const& element)
	{
		XMLVisitor* visitor = getVisitor(element);
		if (visitor)
			return visitor->VisitExit(element);
		return true;
	}

	XMLVisitor* AssetLoader::getVisitor(XMLElement const& element)
	{
		auto visitor = std::find_if(mHandlers.begin(), mHandlers.end(), [&element](auto&& other)
		{
			return std::string(element.Name()) == other->elementName();
		});
		if (visitor != mHandlers.end())
			return visitor->get();
		return nullptr;
	}
}
