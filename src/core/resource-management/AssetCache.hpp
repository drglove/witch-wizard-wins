#pragma once

#include <string>
#include <Thor/Resources.hpp>
#include <Thor/Animations/AnimationMap.hpp>
#include <Thor/Animations/Animator.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>

namespace WizardWars
{
    typedef std::string AssetReference;
    typedef thor::ResourceHolder<sf::Texture, AssetReference> TextureCache;
	typedef thor::AnimationMap<sf::Sprite, std::string> SpriteAnimations;
	typedef thor::Animator<sf::Sprite, std::string> SpriteAnimator;
	typedef thor::ResourceHolder<SpriteAnimations, AssetReference> AnimationCache;

    struct AssetCache
    {
        TextureCache textureCache;
		AnimationCache animationCache;
    };
}
