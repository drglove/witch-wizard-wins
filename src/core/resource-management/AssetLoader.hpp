#pragma once
#include "AssetCache.hpp"

#include <tinyxml2.h>
#include <vector>
#include <memory>

namespace WizardWars
{
	class AssetHandler
		: public tinyxml2::XMLVisitor
	{
	public:
		typedef std::unique_ptr<AssetHandler> Ptr;
		virtual std::string elementName() const = 0;
	};

	class AssetLoader
		: public tinyxml2::XMLVisitor
	{
	public:
		AssetLoader(AssetCache& assetCache);
		void load(std::string const& rootFile);

		///@{XMLVisitor implementation
		using tinyxml2::XMLVisitor::VisitEnter;
		using tinyxml2::XMLVisitor::VisitExit;
		bool VisitEnter(tinyxml2::XMLElement const& element, tinyxml2::XMLAttribute const* firstAttribute) override;
		bool VisitExit(tinyxml2::XMLElement const& element) override;
		///@}

	private:
		void registerHandler(AssetHandler::Ptr handler);
		tinyxml2::XMLVisitor* getVisitor(tinyxml2::XMLElement const& element);

		std::vector<AssetHandler::Ptr> mHandlers;
	};
}
