#include "SpriteAssetHandler.hpp"
#include "utilities/Macros.hpp"

#include <fstream>
#include <Thor/Animations.hpp>

using namespace tinyxml2;

namespace WizardWars
{
	SpriteAssetHandler::SpriteAssetHandler(AssetCache& assets)
		: mAssets(assets)
	{
	}

	std::string SpriteAssetHandler::elementName() const
	{
		return "Sprite";
	}

	bool SpriteAssetHandler::VisitEnter(XMLElement const& element, XMLAttribute const*)
	{
		loadTexture(element);
		loadFrames(element);
		return true;
	}

	void SpriteAssetHandler::loadTexture(XMLElement const& element)
	{
		using namespace thor;
		using namespace sf;
		XMLElement const* textureElement = element.FirstChildElement("Texture");
		ASSERT(textureElement);
		std::string const textureFilename = textureElement->GetText();
		AssetReference const& textureAssetRef = static_cast<AssetReference>(textureFilename);
		TextureCache& textureCache = mAssets.textureCache;
		textureCache.acquire(textureAssetRef, Resources::fromFile<Texture>(textureFilename));
	}

	void SpriteAssetHandler::loadFrames(XMLElement const& element)
	{
		XMLElement const* framesElement = element.FirstChildElement("Frames");
		if (!framesElement)
			return;
		std::string const framesFilename = framesElement->GetText();
		AssetReference spriteAssetRef = element.Attribute("assetReference");
		AnimationCache& animationCache = mAssets.animationCache;
		animationCache.acquire(spriteAssetRef, LoadAnimationMap(framesFilename));
	}

	json SpriteAssetHandler::LoadAnimationsIntoJson(std::string const& fileName)
	{
		std::ifstream framesStream(fileName);
		json j;
		framesStream >> j;
		return j;
	}

	void SpriteAssetHandler::PopulateAnimationMap(json const& animationData, SpriteAnimations& animationMap)
	{
		for (auto const& animDef : animationData["meta"]["frameTags"])
		{
			thor::FrameAnimation animation;
			std::string const animName = animDef["name"];
			sf::Uint32 startFrame = animDef["from"];
			sf::Uint32 endFrame = animDef["to"];

			sf::Time totalDuration = sf::Time::Zero;
			for (sf::Uint32 iFrame = startFrame; iFrame <= endFrame; ++iFrame)
			{
				json const& frameInfo = animationData["frames"][iFrame];
				sf::Uint32 const duration = frameInfo["duration"];
				// Aseprite specifies durations in milliseconds.
				totalDuration += sf::milliseconds(duration);
				json const& frame = frameInfo["frame"];
				sf::IntRect rect(frame["x"].get<sf::Uint32>(),
							     frame["y"].get<sf::Uint32>(),
					             frame["w"].get<sf::Uint32>(),
					             frame["h"].get<sf::Uint32>());
				animation.addFrame(static_cast<float>(duration), rect);
			}

			animationMap.addAnimation(animName, animation, totalDuration);
		}
	}

	thor::ResourceLoader<SpriteAnimations> SpriteAssetHandler::LoadAnimationMap(std::string const& fileName)
	{
		return thor::ResourceLoader<SpriteAnimations>(
			[&fileName]()
			{
			    auto spriteAnims = std::make_unique<SpriteAnimations>();
				json animationData = LoadAnimationsIntoJson(fileName);
				PopulateAnimationMap(animationData, *spriteAnims.get());
				return spriteAnims;
			},
			fileName
		);
	}
}