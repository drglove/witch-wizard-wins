#pragma once
#include "AssetLoader.hpp"
#include "AssetCache.hpp"

#include <json.hpp>
using json = nlohmann::json;

namespace WizardWars
{
	class SpriteAssetHandler
		: public AssetHandler
	{
	public:
		explicit SpriteAssetHandler(AssetCache& assetCache);

		std::string elementName() const override;

		using tinyxml2::XMLVisitor::VisitEnter;
		bool VisitEnter(tinyxml2::XMLElement const& element, tinyxml2::XMLAttribute const* firstAttribute) override;

	private:
		void loadTexture(tinyxml2::XMLElement const& element);
		void loadFrames(tinyxml2::XMLElement const& element);
		static json LoadAnimationsIntoJson(std::string const& fileName);
		static void PopulateAnimationMap(json const& animationData, SpriteAnimations& animationMap);
		static thor::ResourceLoader<SpriteAnimations> LoadAnimationMap(std::string const& fileName);

		AssetCache& mAssets;
	};
}
