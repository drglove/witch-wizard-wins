#include "SceneNode.hpp"
#include "utilities/Macros.hpp"

namespace WizardWars
{
	SceneNode::SceneNode()
		: mParent(nullptr)
		, mDepth(0)
		, mGatheredDepth(0)
		, mHasAssociatedEntity(false)
	{
	}

	SceneNode::SceneNode(Entity entity)
		: mParent(nullptr)
		, mDepth(0)
		, mGatheredDepth(0)
		, mHasAssociatedEntity(true)
		, mEntity(entity)
	{
	}

	SceneNode& SceneNode::attachChild(SceneNode::Ptr child)
	{
		child->mParent = this;
		mChildren.push_back(std::move(child));
		return *mChildren.back();
	}

	SceneNode::Ptr SceneNode::detachChild(SceneNode const& node)
	{
		auto found = std::find_if(mChildren.begin(), mChildren.end(), [&node](Ptr& p)
		{
			return p.get() == &node;
		});
		ASSERT(found != mChildren.end());
		Ptr result = std::move(*found);
		result->mParent = nullptr;
		mChildren.erase(found);
		return result;
	}

	SceneNode* SceneNode::parent() const
	{
		return mParent;
	}

	void SceneNode::gatherTransforms()
	{
		//TODO: Nick: This should check some dirty flags to cull traversal if the nodes haven't changed.
		for (auto& child : mChildren)
		{
			child->mGatheredTransform = mGatheredTransform * child->getTransform();
			child->mGatheredDepth = mGatheredDepth + child->mDepth;
		}
		for (auto& child : mChildren)
			child->gatherTransforms();
	}

	bool SceneNode::hasEntity() const
	{
		return mHasAssociatedEntity;
	}

	Entity SceneNode::entity() const
	{
		return mEntity;
	}
}
