#pragma once

#include <entt/entt.hpp>

namespace WizardWars
{
	using Registry = entt::DefaultRegistry;
	using Entity = Registry::entity_type;
}
