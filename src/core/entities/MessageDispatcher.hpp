#pragma once

#include <entt/signal/dispatcher.hpp>

namespace WizardWars
{
    using Dispatcher = entt::Dispatcher;
}
