#pragma once

#include <SFML/Graphics/Transformable.hpp>
#include <SFML/System/NonCopyable.hpp>
#include "core/entities/Registry.hpp"

#include <memory>
#include <vector>

namespace WizardWars
{
	class SceneNode : public sf::Transformable, private sf::NonCopyable
	{
	public:
		typedef std::unique_ptr<SceneNode> Ptr;

		SceneNode();
		SceneNode(Entity entity);

		SceneNode& attachChild(Ptr child);
		Ptr detachChild(SceneNode const& node);
		SceneNode* parent() const;

		void setDepth(int depth) { mDepth = depth; }

		void gatherTransforms();
		inline sf::Transform const& gatheredTransform() const { return mGatheredTransform; }
		inline int gatheredDepth() const { return mGatheredDepth; }

		bool hasEntity() const;
		Entity entity() const;

	private:
		SceneNode*			mParent;
		std::vector<Ptr>	mChildren;
		sf::Transform		mGatheredTransform;
		int					mDepth;
		int					mGatheredDepth;

		bool				mHasAssociatedEntity;
		Entity				mEntity;
	};
}
