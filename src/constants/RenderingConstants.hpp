#pragma once

#include <cstdint>

namespace WizardWars
{
	// 1m = 50pixels @ 1080p.
	// We want a wizard texture to be 1.8m tall.
	// These are authored on a 32 pixel canvas.
	static float constexpr PhysicsWorldToRenderScale = 50.0f;
	static float constexpr TextureWorldToRenderScale = 1.8f * PhysicsWorldToRenderScale / 32.0f;

	static uint32_t constexpr NativeResolutionHeight = 1080;
	static uint32_t constexpr NativeResolutionWidth = 1920;
	static float constexpr NativeAspectRatio = static_cast<float>(NativeResolutionHeight) / NativeResolutionWidth;
}