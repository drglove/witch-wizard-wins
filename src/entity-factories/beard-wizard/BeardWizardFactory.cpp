#include "BeardWizardFactory.hpp"

#include "core/Game.hpp"
#include "core/resource-management/AssetCache.hpp"

#include "core/components/TransformComponent.hpp"
#include "core/components/SpriteComponent.hpp"
#include "core/components/PlayerInputComponent.hpp"
#include "core/components/CharacterController.hpp"
#include "core/components/AnimationComponent.hpp"

namespace WizardWars
{
	Entity BeardWizardFactory::Create(InitialPlayerControl controls)
	{
		Registry& registry = Game::EntityRegistry();
		AssetCache const& assets = Game::Assets();

		Entity wizard = registry.create();
		registry.assign<TransformComponent>(wizard);
		registry.assign<SpriteComponent>(wizard, sf::Sprite(assets.textureCache["media/characters/beard-wizard/beard-wizard.png"]));
		switch (controls)
		{
		case InitialPlayerControl::NOT_CONTROLLED:
			break;
		case InitialPlayerControl::PLAYER_1_CONTROLS:
			registry.assign<PlayerInputComponent>(wizard, PlayerInputComponent::player1_t{});
			break;
		case InitialPlayerControl::PLAYER_2_CONTROLS:
			registry.assign<PlayerInputComponent>(wizard, PlayerInputComponent::player2_t{});
			break;
		}
		registry.assign<CharacterController>(wizard);
		registry.assign<AnimationComponent>(wizard, &assets.animationCache["beard-wizard"]);
		return wizard;
	}
}