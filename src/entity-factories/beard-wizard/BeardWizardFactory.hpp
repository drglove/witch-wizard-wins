#pragma once
#include "core/entities/Registry.hpp"

namespace WizardWars
{
	class BeardWizardFactory
	{
	public:
		enum class InitialPlayerControl
		{
			NOT_CONTROLLED,
			PLAYER_1_CONTROLS,
			PLAYER_2_CONTROLS,
		};
		static Entity Create(InitialPlayerControl controls = InitialPlayerControl::NOT_CONTROLLED);
	};
}