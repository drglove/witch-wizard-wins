#pragma once
#include "core/entities/Registry.hpp"
#include <SFML/System/Vector2.hpp>

namespace WizardWars
{
	class LaserFactory
	{
	public:
		static Entity Create(sf::Vector2f source, sf::Vector2f target);
	};
}