#include "LaserFactory.hpp"

#include "core/Game.hpp"
#include "core/resource-management/AssetCache.hpp"

#include "core/components/TransformComponent.hpp"
#include "core/components/SpriteComponent.hpp"
#include "core/components/LaserComponent.hpp"

namespace WizardWars
{
	Entity LaserFactory::Create(sf::Vector2f source, sf::Vector2f target)
	{
		Registry& registry = Game::EntityRegistry();
		AssetCache const& assets = Game::Assets();
		(void)assets;

		Entity laser = registry.create();
		auto& transformComponent = registry.assign<TransformComponent>(laser);
		transformComponent.setPosition(source);
		registry.assign<LaserComponent>(laser, target, 1.0f);
		return laser;
	}
}