#pragma once

#include <SFML/Graphics/Transform.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

namespace WizardWars
{
	sf::Transform WorldToScreenCoords(sf::RenderTarget const& renderTarget);
}