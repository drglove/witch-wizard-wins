#include "RenderingUtilities.hpp"
#include "constants/RenderingConstants.hpp"

namespace WizardWars
{
	sf::Transform WorldToScreenCoords(sf::RenderTarget const& renderTarget)
	{
		sf::Transform scaleXform;
		float const scale = PhysicsWorldToRenderScale * static_cast<float>(renderTarget.getSize().y) / NativeResolutionHeight;
		scaleXform.scale({ scale, -scale });
		sf::Transform translateXform;
		translateXform.translate({ 0.0f, static_cast<float>(renderTarget.getSize().y) });
		return translateXform * scaleXform;
	}
}