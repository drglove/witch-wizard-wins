#pragma once

#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable: 4668)
#include <windows.h>
#pragma warning(pop)
#endif

#if defined(DEBUG)
#define ASSERT(expr) (void)(expr ? 0 : ::WizardWars::AssertMsg(__FILE__, __LINE__, #expr))
#define ASSERT_MSG(expr, ...) (void)(expr ? 0 : ::WizardWars::AssertMsg(__FILE__, __LINE__, __VA_ARGS__))
#else
#define ASSERT(expr) (void)(0)
#define ASSERT_MSG(expr, ...) (void)(0)
#endif
#define ASSERT_UNREACHABLE {ASSERT(false); ::exit(EXIT_FAILURE)}
#define ASSERT_UNREACHABLE_MSG(...) {ASSERT_MSG(false, __VA_ARGS__); ::exit(EXIT_FAILURE)}

namespace WizardWars
{
	inline void AssertMsg(char const* file, long line, char const* fmt, ...)
	{
#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable: 4365)
#pragma warning(disable: 4774)
		static const uint32_t bufferSize = 8 * 1024;
		char buffer[bufferSize];
		char* bufferStart = buffer;
		char* bufferEnd = &buffer[bufferSize];
		snprintf(bufferStart, bufferEnd - bufferStart, "%s(%ld) : Assertion failed : ", file, line);
		bufferStart += strlen(bufferStart);

		va_list argList;
		va_start(argList, fmt);
		snprintf(bufferStart, bufferEnd - bufferStart, fmt, argList);
		bufferStart += strlen(bufferStart);
		va_end(argList);

		bufferStart[0] = '\n';
		bufferStart[1] = 0;

		OutputDebugStringA(buffer);
		if (IsDebuggerPresent())
			DebugBreak();
#pragma warning(pop)
#endif
	}
}
