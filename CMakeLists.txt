cmake_minimum_required (VERSION 3.7.0)
project (wizardwars)

set (CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})
set (CMAKE_EXPORT_COMPILE_COMMANDS ON)
set (CMAKE_CXX_STANDARD 14)
set (CMAKE_CXX_STANDARD_REQUIRED ON)

file (GLOB_RECURSE sources src/*.cpp src/*.hpp)
file (GLOB_RECURSE media media/*)

set (CMAKE_MACOSX_RPATH 1)

# SFML
include_directories (3rdparty/SFML/include)
add_subdirectory (3rdparty/SFML)

# Set these for Thor so that it does not try to find pre-compiled libraries.
set (SFML_FOUND TRUE)
set (SFML_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/3rdparty/SFML/include")
set (SFML_LIBRARIES "sfml-audio" "sfml-graphics" "sfml-window" "sfml-system")
include_directories (3rdparty/Thor/include)
include_directories (3rdparty/Thor/extlibs/aurora/include)
add_subdirectory (3rdparty/Thor)

# Box2D for Physics
include_directories (3rdparty/Box2D)
file (GLOB_RECURSE box2dsources 3rdparty/Box2D/Box2D/*.cpp)

# XML parsing
file (GLOB tinyxmlsources 3rdparty/tinyxml2/tinyxml2.cpp)
include_directories (3rdparty/tinyxml2)

# JSON parsing (header only)
include_directories (3rdparty/json/src)

# Entity management (header only)
include_directories (3rdparty/entt/src)

if (MSVC)
	add_compile_options (/WX /W4)
	string(REGEX REPLACE " /W[0-4]" "" CMAKE_C_FLAGS "${CMAKE_C_FLAGS}")
	string(REGEX REPLACE " /W[0-4]" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
else ()
	add_compile_options (-Wall)
endif ()
add_executable (wizardwars ${sources} ${media})

target_link_libraries (wizardwars sfml-audio sfml-graphics sfml-window sfml-system thor)
if (WIN32)
    target_link_libraries (wizardwars sfml-main)
endif ()
target_sources (wizardwars PUBLIC ${tinyxmlsources} ${box2dsources})
target_include_directories (wizardwars PUBLIC src)

add_custom_command(TARGET wizardwars POST_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                   ${CMAKE_SOURCE_DIR}/media $<TARGET_FILE_DIR:wizardwars>/media)

if (MSVC)
	set_target_properties (wizardwars PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "\$(SolutionDir)\$(Configuration)")
	set_property (DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT wizardwars)
endif ()

install (TARGETS wizardwars RUNTIME DESTINATION build)
install (DIRECTORY media DESTINATION build)

set (CPACK_PACKAGE_NAME "wizardwars")
set (CPACK_PACKAGE_VERSION "1.0.0")
set (CPACK_MONOLITHIC_INSTALL 1)

include (CPack)
